import React, { useEffect } from "react";
import "antd/dist/antd.css";
import { Table } from "antd";
import urls from "../rest-api";

function RecogProfile() {
  const dataSource = [];

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Project ID",
      dataIndex: "projectid",
      key: "projectid",
    },
  ];

  const getRandomuserParams = (params) => ({
    results: params.pagination.pageSize,
    page: params.pagination.current,
    ...params,
  });

  //useEffect
  useEffect(() => {
    console.log("Get data recog profile");
    fetch(urls.getAllrecogProfile, {
      method: "GET",
      headers: {
        "Content-Type": "application/vnd.api+json",
      },
    })
      .then((response) => response.json())
      .then((repos) => {
        console.log(repos);
        var data = repos.data;
        debugger;
        for (var i = 0; i < data.length; i++) {
          dataSource.push({
            id: data[i].id,
            name: data[i].attributes.name,
            projectid: data[i].attributes.id,
          });
        }
      })
      .catch((err) => console.log(err));
  });
  return <Table dataSource={dataSource} columns={columns} />;
}

export default RecogProfile;
