import Project from "../components/project/Project";
import RecogProfile from "../components/recog-profile/RecogProfile";

const routes = [
  {
    path: "/project",
    component: Project,
    breadcrumb: "project",
  },
  ,
  {
    path: "/ip-project",
    component: RecogProfile,
    breadcrumb: "ip-project",
  },
  {
    path: "/ip-flow",
    component: RecogProfile,
    breadcrumb: "ip-flow",
  },
  {
    path: "/recog-profile",
    component: RecogProfile,
    breadcrumb: "recog-profile",
  },
  {
    path: "/templates",
    component: RecogProfile,
    breadcrumb: "templates",
  },
  {
    path: "/extraction",
    component: RecogProfile,
    breadcrumb: "extraction",
  },
  {
    path: "/testcase",
    component: RecogProfile,
    breadcrumb: "testcase",
  },
];
export default routes;
