import { GET_ALL_PROJECT } from "../types";

const initialState = {
  todos: [
    {
      name: "hung",
    },
  ],
};

const todoReducer = (state = initialState, action) => {
  console.log("hung");
  switch (action.type) {
    case GET_ALL_PROJECT:
      return {
        ...state,
        todos: action.payload,
      };

    default:
      return state;
  }
};

export default todoReducer;
